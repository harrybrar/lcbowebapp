<%@ page import ="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liquor Store</title>
   	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<!-- <link rel="stylesheet" href="css/bootstrap.min.1.4.0.css" > -->
</head>
<body>
<div class="container4">
	<center>
		<h1>
		    Available Brands
		</h1>
		<%
		List result= (List) request.getAttribute("brands");
		Iterator it = result.iterator();
		out.println("<br>At LCBO, We have <br><br>");
		while(it.hasNext()){
		out.println(it.next()+"<br>");
		}
		%>
		<form action="index.html">
			<input type="submit" value="Home"/> 
		</form>
	</center>
</div>
</body>
</html>